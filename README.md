Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [License](#license)
4. [Author](#author-information)

Description
=========

Repo provides a sample RESTfull API application with go frontend. 
The build pipeline is creating and uploading docker image to the registry (dockerhub).
Application is serving HTTP RESTfull API endpoint. 
The idea is create a pipeline for all necessary go language needs. 

Pipeline
------------

A multistage gitlab-ci pipeline is built using docker as executor and consists of:
  - test
        simple verification of go linting rules, format and etc
  - build
        compile the binary from source code 
  - smoke_test
        executes the smoke tests based on go tests (file smoke_test.yml)
  - test_dockerfile
        executes the conftest test of Dockerfile. All credits goes to [conftest community](#https://www.conftest.dev/) 
  - image_build
        build of docker image with [kaniko](#https://github.com/GoogleContainerTools/kaniko) and publish it to dockerhub 
  - docker-scan
        security validation with [trivy](#https://github.com/aquasecurity/trivy) tool 


License
-------

GNU GPL license


Author Information
------------------

Andrii KOSTENETSKYI
