package main

import (
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"fmt"
	"os"
)

func main() {
    hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("Hostname: %s", hostname)


	r := gin.Default()

	r.GET("/hello", func(c *gin.Context) {
		c.String(200, "Hello, friend! This is version 2.3 " + "  Hostname: %s", hostname)
	})

	api := r.Group("/api")

	api.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.Use(static.Serve("/", static.LocalFile("./views", true)))

	r.Run()
}