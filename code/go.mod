module digitalronin/helloworld

go 1.13

require (
	github.com/bluehoodie/smoke v1.1.0 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.7.2
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
